
const mongo_connection = 'mongodb://mongo:27017/local'
const zk_connection = 'zookeeper:2181/'
const offsets_topic = 'my-debezium_offsets'
const ns = 'test.*'

const debezium = require('../lib/debezium')({
	mongo_connection,
	zk_connection
})

debezium({
	id: 'some-test',
	ns,
	// map_fn: ({o}) => o,
	topic: 'helo'
})