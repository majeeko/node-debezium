const bson = require('bson')
const offset_manager_lib = require('../lib/offset')
const {Producer} = require('sinek')
const _ = require('lodash')
const Oplog = require('@majeeko/rx-mongo-oplog')
const { Observable } = require('rxjs')

const debounce_interval = process.env.DEBOUNCE_MS ? parseInt(process.env.DEBOUNCE_MS) : 200
const max_items = process.env.MAX_ITEMS ? parseInt(process.env.MAX_ITEMS) : 50

function regex(pattern) {
	pattern = pattern || '*';
	pattern = pattern.replace(/[*]/g, '(.*?)');
	return new RegExp('^' + pattern + '$', 'i');
}

const default_map_fn = ({_id, log}) => ({ value: log })

const get_id = log => _.get(log, 'o2._id', _.get(log, 'o._id'))

function getOffsetTimestamp(offset) {
	if(offset) {
		try {
			return bson.Timestamp.fromString(JSON.parse(offset).ts)
		} catch(err) {
			console.error(`Bad offset:`, JSON.parse(offset))
		}
	}
	return null
}

function init({
	id, // *
	mongo_connection, // *
	zk_connection, // *
	kafka_offsets_topic
}) {
	let producers = {}

	function getProducer(topic) {
		if(!producers[topic]) {
			const producer = new Producer({zkConStr: zk_connection}, topic)
			producers[topic] = producer.connect().then(() => {
				console.log(`Kafka producer for topic '${topic}' connected`)
				return producer
			})
		}
		return producers[topic]
	}

	async function sendMessage(topic, value, key) {
		const producer = await getProducer(topic)
		return key ? producer.buffer(topic, key, value) : producer.send(topic, JSON.stringify(value))
	}

	async function commitOffset(id, offset) {
		return offset_manager.updateOffset(id, offset)
	}

	if(!id || !mongo_connection || !zk_connection) throw new Error(`missing required params (some of: id, mongo_connection, zk_connection)`)
	const offset_manager = offset_manager_lib({mongo_connection, offsets_topic: kafka_offsets_topic})
	return function({
		ns, // *
		topic,
		map_fn,
		include_payload
	}) {
		if(!ns) throw new Error(`missing required param 'ns'`)
		console.log(id, ns)
		let subscription
		offset_manager.getOffset(id)
			.then(offset => {
				console.log(`Offset for connector ${id} is ${offset}`)
				let ts = getOffsetTimestamp(offset)

				let counter = 0
				setInterval(() => {
					!counter && console.log('No oplogs to commit')
					counter = 0
				}, 10000)

				const oplog = Oplog(mongo_connection, ns, ts)
				subscription = oplog
					.map(log => {
						const _id = get_id(log)
						const _topic = topic || `${id}_${log.ns}`
						const {key, value} = (map_fn || default_map_fn)({_id, log})
						const offset = { ts:log.ts.toString(), h:log.h.toString() }
						
						return {topic:_topic, key, value, offset}
					})
					.flatMap(({topic, key, value, offset}) => 
						Observable.defer(() => sendMessage(topic, value, key))
						.map(() => offset))
					.do(() => ++counter)
					.bufferTime(500)
					.map(_.last)
					.subscribe(
						offset => {
							if(offset) {
								console.log(`Committed ${counter} oplogs`)
								commitOffset(id, offset)
							}
						},
						console.error
					)
			})
			.catch(err => {
				console.error(err)
				throw err
			})
		
		return {
			close: () => subscription && subscription.unsubscribe()
		}
	}
}

module.exports = init