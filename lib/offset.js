const mongo = require('then-mongo')
const _ = require('lodash')
const { parseMongoConnectionString } = require('./util')

function formatMongoConnection(mongo_connection, offsets_topic) {
	if(!offsets_topic) throw new Error('Missing offsets_topic')
	const {hosts, params} = parseMongoConnectionString(mongo_connection)
	return `mongodb://${hosts}/${offsets_topic}${params || ''}`
}

function init({mongo_connection, offsets_topic = '__node_debezium_offsets'}) {
	const coll = mongo(formatMongoConnection(mongo_connection, offsets_topic), ['offsets'])['offsets']

	async function getOffsetsMap() {
		return coll.find({}).then(offsets => offsets.reduce((acc, next) => _.set(acc, next._id, next.offset), {}))
	}

	async function getOffset(_id) {
		return coll.findOne({_id}).then(offset => offset ? offset.offset : null)
	}

	async function updateOffset(_id, offset) {
		return coll.update({_id}, {$set: {offset: JSON.stringify(offset)}}, { upsert: true })
	}

	return {
		getOffsetsMap,
		getOffset,
		updateOffset
	}
}

module.exports = init
