function parseMongoConnectionString(mongo_connection) {
	const [, , _hosts, , name, params] = mongo_connection.match(/^(mongodb:\/\/)?([^\/]+)+(\/([\w]+))?(\?.*)?$/) || []
	const hosts = _hosts.split(',')
	if(!hosts) throw new Error('Bad mongo connection string')
	return { hosts, name, params }
}

module.exports = {
	parseMongoConnectionString
}