# node-debezium

Actually this is just a MongoDB oplog connector to Kafka.

### Todo

- fix consumer offset name
- add option to apply projection on oplogs (to reduce traffic when dealing with big documents)