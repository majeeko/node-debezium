const offsets_topic = 'my-debezium_offsets'
const zkConStr = 'zookeeper:2181/'
const offset = require('../lib/offset')({zkConStr})

// offset.getOffsetsMap().then(console.log)
// offset.getOffset('someconsumerid').then(console.log)
// offset.updateOffset('someconsumerid', {"helo":"pelo"}).then(console.log)


offset.updateOffset('someconsumerid', {"h":"-4512101495614180434","ts":"6437389544084996097"})
	.then(() => offset.getOffsetsMap())
	.then(console.log)